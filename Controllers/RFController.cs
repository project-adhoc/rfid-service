﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RFID_Service.Models;
using RFID_Service.Services;

namespace RFID_Service.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class RFController : ControllerBase
    {
        private readonly IRFService _service;
        private readonly IUserService _user;
        public RFController(IRFService rfService, IUserService user)
        {
            _service = rfService;
            _user = user;
        }

        [AllowAnonymous]
        [HttpPost("authenticate")]
        public async Task<IActionResult> Authenticate([FromBody]Users model)
        {
            if (model == null)
                return BadRequest(new { message = "Authentication info is null" });

            if (string.IsNullOrWhiteSpace(model.Username))
                return BadRequest(new { message = "Username is empty" });

            if (string.IsNullOrWhiteSpace(model.Password))
                return BadRequest(new { message = "Password is empty" });

            Users user;

            try
            {
                user = await _user.Authenticate(model.Username, model.Password);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
                // return BadRequest(new { message = ex.Message });
            }

            // No user found.
            if (user == null)
                return NotFound("Username or password is incorrect");

            return Ok(user);
        }

        [HttpGet]
        public async Task<List<RFModel>> Get()
        {
            var model = await _service.Get();
            return model;
        }

        // rf/filter?datefrom=2020-01-11&dateto=2020-01-12
        [HttpGet("Filter")]
        public async Task<List<RFModel>> GetByDate([FromQuery] DateRange dateRange)
        {
            var model = await _service.GetByDate(dateRange.DateFrom, dateRange.DateTo);
            return model;
        }

        [HttpGet("UserWithoutExit")]
        public async Task<List<RFModel>> GetUserWithoutExit(DateTime dateFrom, DateTime dateTo)
        {
            var model = await _service.GetUserWithoutExit(dateFrom, dateTo);
            return model;
        }

        [HttpGet("doors")]
        public async Task<List<DoorModel>> GetDoors()
        {
            var model = await _service.GetDoors();
            return model;
        }
    }
}