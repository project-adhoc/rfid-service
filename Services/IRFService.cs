﻿using RFID_Service.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RFID_Service.Services
{
    public interface IRFService : IDisposable
    {
        Task<List<RFModel>> Get();
        Task<List<RFModel>> GetByDate(DateTime dateFrom, DateTime dateTo);
        Task<List<DoorModel>> GetDoors();
        Task<List<RFModel>> GetUserWithoutExit(DateTime dateFrom, DateTime dateTo);
    }
}
