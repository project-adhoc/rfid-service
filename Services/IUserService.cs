﻿using RFID_Service.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RFID_Service.Services
{
    public interface IUserService
    {
        Task<Users> Authenticate(string username, string password);
    }
}
