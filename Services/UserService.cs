﻿using Microsoft.Extensions.Configuration;
using RFID_Service.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RFID_Service.Services
{
    public class UserService : IUserService
    {
        private readonly RFContext _context;
        private readonly IConfiguration _configuration;
        public UserService(RFContext context, IConfiguration configuration)
        {
            _context = context;
            _configuration = configuration;
        }
        private bool _disposed;
        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            _disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        public async Task<Users> Authenticate(string username, string password)
        {
            var uname = _configuration["BasicAuth:Username"];
            var pword = _configuration["BasicAuth:Password"];
            var user = new Users();
            if (uname.ToLower()==username.ToLower() && pword.ToLower() == password.ToLower())
            {
                user = new Users
                {
                    Username = username,
                    Password = password
                };
            }
            return await Task.FromResult(user);
        }
    }
}
