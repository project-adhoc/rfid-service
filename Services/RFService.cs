﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using RFID_Service.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RFID_Service.Services
{
    public class RFService : IRFService
    {
        private readonly RFContext _context;

        public RFService(RFContext context)
        {
            _context = context;
        }
        private bool _disposed;

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        public async Task<List<RFModel>> Get()
        {
            var rfData = await _context.RFData.ToListAsync();
            return rfData;
        }

        public async Task<List<RFModel>> GetByDate(DateTime dateFrom, DateTime dateTo)
        {
            var date = ConvertDate(dateFrom, dateTo);
            dateFrom = date.DateFrom;
            dateTo = date.DateTo;
            var rfData = await _context.RFData.Where(d=> d.LocalTime >= dateFrom && d.LocalTime <= dateTo).ToListAsync();
            return rfData;
        }

        public async Task<List<DoorModel>> GetDoors()
        {
            //// temporary query
            var doors = await _context.RFData.Select(d => new DoorModel
            {
                Door = d.Door
            }).Distinct().ToListAsync();
            
            return doors;
        }

        public async Task<List<RFModel>> GetUserWithoutExit(DateTime dateFrom, DateTime dateTo)
        {
            var date = ConvertDate(dateFrom, dateTo);
            dateFrom = date.DateFrom;
            dateTo = date.DateTo;
            var inList = await _context.RFData.Where(d => d.Direction.ToUpper()=="IN" && d.LocalTime >= dateFrom && d.LocalTime <= dateTo).ToListAsync();
            var outList = await _context.RFData.Where(d => d.Direction.ToUpper() == "OUT" && d.LocalTime >= dateFrom && d.LocalTime <= dateTo).ToListAsync();
            inList.RemoveAll(item => outList.Any(d => d.CardNo == item.CardNo));
            return inList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dateFrom"></param>
        /// <param name="dateTo"></param>
        /// <returns></returns>
        private DateRange ConvertDate(DateTime dateFrom, DateTime dateTo)
        {
            return new DateRange
            {
                DateFrom = Convert.ToDateTime($"{dateFrom.ToString("yyyy-MM-dd")} 00:00:00"),
                DateTo = Convert.ToDateTime($"{dateTo.ToString("yyyy-MM-dd")} 23:59:59")
            };
        }
    }

}
