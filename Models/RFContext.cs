﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RFID_Service.Models
{
    public class RFContext : DbContext
    {
        public RFContext(DbContextOptions<RFContext> options) : base(options)
        {
        }
        public DbSet<RFModel> RFData { get; set; }
        public DbSet<Users> Users { get; set; }
    }
}
